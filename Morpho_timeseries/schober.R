#IN THIS SCRIPT I TRY TO IMPLEMENT THE ideas exposed in the review from Schober and Vetter 2018


library(tidyverse)
library(nlme)



#First comparison: Difference between the two wild types COL (A. thaliana) and OX (C. hirsuta)
#loading data from experiment
experiment=read.table("input/DataFilesSoeren/db_soeren_201907.csv",T, sep=",")

#show variable names
names(experiment)
#show dimension
dim(experiment)
#show summary
summary(experiment)

pdf(file = "output_WT_Col_vs_WT_ox.pdf")

# create new variable to ensure uniqueness of group names 
experiment <- experiment %>%
  unite(Sample_unique, Genotype, Sample, remove=F) %>%
  select(-Sample)

#subsetting genotypes
#always check min/max values with A. Runions
#this is where filtering is done. Here we filter out WT-Ox_8 because it only has 3 time points, which introduces missing data for the repeated measures ANOVA
df1=experiment %>%
  filter(Genotype %in% c("WT-Col","WT-Ox")) %>%
  filter(Growth >= 0.9, Growth < 10, Sample_unique!="WT-Ox_8", Timepoint <= 4)

#we model the average growth rate per leaf as a function of the species, the time, and the accession
df1_mean = df1 %>% group_by(Genotype,Sample_unique,Timepoint) %>% dplyr::summarise(ave_growth=mean(Growth, na.rm=T),
                                                                                   ave_ga=mean(Geometry_Area, na.rm=T),
                                                                                   ave_prolif=mean(Prolif, na.rm=T))  

#here following "The R book, p. 642)
grouped_df1_mean_growth=groupedData(ave_growth~Timepoint|Sample_unique, outer = ~Genotype, data=df1_mean)
grouped_df1_mean_geom_area=groupedData(ave_ga~Timepoint|Sample_unique, outer = ~Genotype, data=df1_mean)
grouped_df1_mean_growth_prolif=groupedData(ave_prolif~Timepoint|Sample_unique, outer = ~Genotype, data=df1_mean)

#one possible visualization of all mean values
plot(grouped_df1_mean_growth, main="growth", outer=T)
plot(grouped_df1_mean_geom_area, main="geom_area", outer=T)
plot(grouped_df1_mean_growth_prolif, main="proliferation", outer=T)


dev.off()
