---
title: "Growth Analyses Col-WT versus OX-WT"
author: "Yasir Arafat Tamal"
date: "3/23/2020"
output: 
  html_document: 
    fig_height: 9
    fig_width: 11
editor_options: 
  chunk_output_type: console
---

<!-- **Cellular growth analyses: Col-0 WT versus Ox WT** -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
<br/>
Analysis of Variance (ANOVA) comes in many shapes and sizes. It allows to test whether the subjects perform differently in different experimental conditions or species. This report will focus on Two-Way Mixed ANOVA.   

### **Two-way mixed ANOVA**
Two-way mixed ANOVA compares the mean differences between groups that have been split on two "factors" or independent variables, where one factor is a "within-subjects" factor and the other factor is a "between-subjects" factor. A two-way mixed ANOVA is often used in studies where a dependent variable or continuous variable is measured (e.g., growth) over two or more timepoints or when all subjects have undergone two or more conditions (i.e., where "time" or "conditions" are "within-subjects" factor), but also when the subjects have been assigned into two or more separate groups (e.g., based on some characteristic, such as subjects' "genotype" or "gender", or when they have undergone different interventions). These groups form the "between-subjects" factor. In this study,   

  * Within-subjects factor (which have related categories also known as repeated measures) - Timepoint,   
  
  * Between-subjects factor (which have independent categories) - Genotype.   


### **Why using two-way mixed ANOVA?**

The term ‘Two-Way’ gives an indication of how many Independent Variables can be there in the experimental design. In this case: two.  

The term ‘Mixed’ indicates the nature of these variables. While a ‘repeated-measures ANOVA’ contains only within subjects variables (where subjects take part in all conditions) and an ‘independent ANOVA’ uses only between subjects variables (where subjects only take part in one condition), 'Mixed ANOVA'
contains both variable types. In this case, one of each (one within-subjects and one between-subjects).  

Two-way mixed ANOVA is ofetn used for studies to investigate whether any change in the dependent variable (growth) is the result of the interaction between the between-subjects factor (independent variable - Genotype) and within-subjects factor (independent variable - Timepoint). 


### **Purpose**

Two-way mixed ANOVA is used to compare the means of groups cross-classified by two different types of factor variables (one within-subjects factor and one between-subjects factor). The primary purpose of a two-way mixed ANOVA is to understand if there is an interaction (one within-subjects factor * one between-subjects factor) between these two factors on the dependent variable. 




### **Goal**

The goal of this analysis is to understand if there is an interaction between Timepoint (within-subjects factor) and Genotype (between-subjects factor) on the growth of the cells.


  
### **Introduction to the time series data**

We want to find out whether the growth rate of *Cardamine hirsutra* and *Arabidopsis theliana* differs over time. Therefore, the dependent variable is "Growth", whilst the within-subjects factor is "Timepoint" and the between-subjects factor is "Genotypes". More specifically, the two different species, which are known as "Genotypes", are *Cardamine hirsutra* and *Arabidopsis theliana*. These two species reflect the two groups of the "between-subjects" factor.

```{r echo=FALSE, results= 'hide', message=FALSE, warning=FALSE}

library(tidyverse)
library(ggpubr)
library(rstatix)
library(datarium)

#First comparison: Difference between the two wild types Col-0 (A. thaliana) and Ox (C. hirsuta)
#loading data from experiment
experiment=read.table("input/DataFilesSoeren/db_soeren_201907.csv",T, sep=",")

# create new variable to ensure uniqueness of group names 
experiment <- experiment %>%
  unite(Sample, Genotype, Sample, remove=F) %>%
  select(c(-Label, -Parent,-Lobeyness, -Prolif, -Geometry_Area))

summary(experiment)
```
<br/>
Summary of the data:
```{r, echo = FALSE, message=FALSE}
#subsetting genotypes
#always check min/max values with A. Runions 
df1=experiment %>%
  filter(Genotype %in% c("WT-Col","WT-Ox"))

summary(df1)
```
<br/>
There are many missing values for the growth measurement and have been removed from the data. If the growth of a cell is smaller than 0.9 or larger than 10, it has been removed from the data (according to A. Runions).  

Dimension of the data after filtering out cells:
```{r, echo = FALSE, message=FALSE}
#subsetting genotypes
#always check min/max values with A. Runions 
df1=df1 %>%
  filter(Growth >= 0.9, Growth < 10)

#Dimension
dim(df1)

```
<br/>
Showing some random rows of the data by groups.
```{r, echo = FALSE, message=FALSE}
set.seed(123)
df1 %>% sample_n_by(Sample, Timepoint, size = 1) 

```
<br/>
The dataset contains the growth value, measured at several timepoints, of two groups of leaf samples (grp 1: WT-Ox, grp 2: WT-Col). In the experiment, the growth value of the cells is measured in seven leaf samples over time. Of these seven leaves, three *Arabidopsis thaliana* (WT-Col) leaves and 4 *Cardamine hirsuta* leaves. Growth value of the cells is measured at several timepoints, which represents the levels of the "within-subjects" factor, "Timepoint" (i.e., growth value is measured at Timepoint 0, timepoint 1 ....).  

The seven leaves:
```{r, echo=FALSE, message=FALSE}
unique(df1$Sample)

```
<br/>
For the genotypes, average growth of each leaf has been calculated at each timepoint (withing-subjects levels).
```{r, echo=FALSE, message=FALSE}
#calculating average growth per leaf
df1_mean = df1 %>% group_by(Genotype,Sample,Timepoint) %>% dplyr::summarise(ave_growth=mean(Growth, na.rm=T))  

df1_mean

```
<br/> The leaves for the genotypes contain different levels of timepoint:
```{r, echo=FALSE, message=FALSE}
table(df1_mean$Sample)
```
<br/>
```{r echo=FALSE, message=FALSE}
#get mean average growth and sd average growth per group x timepoint
#this allows to identify groups whith less than 3 replicates
#this step requires manual inspection of missing data and an ad-hoc choice for filtering the Timepoint variable
df1_mean %>%
  group_by(Timepoint, Genotype) %>%
  get_summary_stats(ave_growth, type = "mean_sd")
```
<br/>
The growth value has been measured at timepoint 6 for two WT-Col (WT-Col_4, WT-Col_7) leaves and one WT-Ox leaf (WT-Ox_1) and at timepoint 7 for one WT-Ox (WT-Ox_1). To compare the mean difference between the genotypes at each timepoint, the mentioned timepoints have been removed from the data, so we have at least three leaves at each timepoint and for the statistical power.
```{r echo=TRUE}
#filter to keep groups with enough observations
df1_mean_filtered=df1_mean %>%  filter(Timepoint %in% c(0,1,2,3,4))

#visualise data
bxp <- ggboxplot(
  df1_mean_filtered, x = "Timepoint", y = "ave_growth",
  color = "Genotype", palette = "jco", add="point"
)
bxp
```
<br/>
Validation of Assumptions:

```{r echo=TRUE}
# Identification of outliers,

df1_mean_filtered %>% group_by(Timepoint, Genotype) %>% identify_outliers(ave_growth)
# no outliers

# Normality assumption; Normality assumption has been checked for each combinations of factor levels - Genotype * Timepoint
df1_mean_filtered %>% group_by(Timepoint, Genotype) %>% shapiro_test(ave_growth)

# At each level of timepoint the homogeneity of variance assumption is checked between Genotypes.
df1_mean_filtered %>% group_by(Timepoint) %>% levene_test(ave_growth ~ Genotype)

# Homogeneity of covariances assumption,
box_m(df1_mean_filtered[, "ave_growth", drop = FALSE], df1_mean_filtered$Genotype)

# The test is not statistically significant - p > 0.001; so equal covariances and have not violated the assumption of homogeneity of covariances.
# If this test is statistically significant (i.e., p < 0.001), you do not have equal covariances.

# Assumption of sphericity; is automatically checked during the computation of the ANOVA test
#get_anova_table(res.aov)
```



```{r}
# Two-way mixed ANOVA test
res.aov <- anova_test(
  data = ungroup(df1_mean_filtered), dv = ave_growth, wid = Sample,
  between = Genotype, within = Timepoint
)
get_anova_table(res.aov)
```


```{r}
# Effect of group at each time point
one.way <- df1_mean_filtered %>%
  group_by(Timepoint) %>%
  anova_test(dv = ave_growth, wid = Sample, between = Genotype) %>%
  get_anova_table() %>%
  adjust_pvalue(method = "bonferroni")
one.way
```


```{r}
# Pairwise comparisons between group levels
pwc <- df1_mean_filtered %>%
  group_by(Timepoint) %>%
  pairwise_t_test(ave_growth ~ Genotype, p.adjust.method = "bonferroni")
pwc
```






